import React, { Component } from "react";

import { hasParameter, transformTimestamp, format, getLast3Month, getLast6Month } from "../../utils/util";
import { withRouter } from "react-router-dom";
import "./index.scss";
import axios from "axios";
import API from "../../constants/api";
import echarts from "echarts/lib/echarts"; // 引入 ECharts 主模块
import "echarts/lib/chart/bar"; // 引入柱状图
import "echarts/lib/chart/line"; // 引入折线图
import "echarts/lib/chart/pie"; // 引入饼图
import "echarts/lib/component/tooltip"; // 引入提示框和标题组件
import "echarts/lib/component/legend"; //图例
import "echarts/lib/component/title";
import { Button, Radio, Input, Select, DatePicker, TreeSelect } from "antd";
import { Calendar } from "antd-mobile";
import moment from "moment"; //时间插件
import locale from "antd/lib/date-picker/locale/zh_CN";
import "moment/locale/zh-cn";
import zhCN from "antd-mobile/lib/calendar/locale/zh_CN";
import Page from "../../components/Page/Page";

moment.locale("zh-cn");
axios.defaults.withCredentials = true;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { TreeNode } = TreeSelect;
// 获取前一天的时间
const dateFormat = "YYYY-MM-DD"; // 定义你需要的时间格式
const defultFormat = "YYYY-MM-01"; //默认为当月1号
// var date = new date();
// var month = date.getMonth() + 1;
// var day = date.getDate();
// var year = date.get;
const defultStart = moment().subtract("1", "day").format(defultFormat);
const teaday = moment().subtract("0", "day").format(dateFormat);
const extra = {
  "2017/07/15": { info: "Disable", disable: true },
};

const now = new Date();

Object.keys(extra).forEach((key) => {
  const info = extra[key];
  const date = new Date(key);
  if (!Number.isNaN(+date) && !extra[+date]) {
    extra[+date] = info;
  }
});
let dataArr = transformTimestamp(now, "-").split("-");
const deStart = dataArr[0] + "-" + dataArr[1] + "-01";
const deEnd = transformTimestamp(now, "-");
// import LineMarkerEcharts from "../../components/LineMarkerEcharts";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataType: 1, //按月还是按日
      timeType: "0", //时间类型
      tmcNo: "",
      corpNo: "",
      tradeStartTime: deStart, //开始时间
      tradeEndTime: deEnd, //结束时间
      payType: "0", //支付方式
      proType: ["1", "2", "3", "4", "5", "6"], //产品类型
      pickType: "date", //时间选择器类型
      treeData: [],
      proTypeValue: [
        { title: "全部", value: "0", active: true },
        { title: "国内机票", value: "1", active: false },
        { title: "国际机票", value: "2", active: false },
        { title: "国内酒店", value: "3", active: false },
        { title: "国际酒店", value: "4", active: false },
        { title: "火车票", value: "5", active: false },
        { title: "用车", value: "6", active: false },
      ],
      deptInfo: [], //部门信息
      config: {
        locale: zhCN,
      },
      mShow: false, //是否展示移动端日历
      depArr: [],
      resArr: [], //放所有的一级部门，重置时候用
      dataInfo: {
        allOrder: "",
        order: "",
        allConsume: "",
        consume: "",
        allAmount: "",
        amount: "",
        allAverage: "",
        average: "",
      },
      isSenior: false,
      road: "",
      source: "",
      orderUserNo: "",
    };
  }

  componentDidMount() {
    // 第一次掉查询接口
    let tmcNo = hasParameter("tmcno", this.props);
    let corpNo = hasParameter("cropno", this.props);
    let road = hasParameter("road", this.props);
    let source = hasParameter("source", this.props);
    let orderUserNo = hasParameter("orderUserNo", this.props);
    this.setState(
      {
        tmcNo: tmcNo,
        corpNo: corpNo,
        road: road,
        source: source,
        orderUserNo: orderUserNo,
      },
      () => {
        if (source === "admin") {
          this.genTreeNode(corpNo, corpNo).then((res) => {
            console.log("res", res);

            var resArr = [];
            for (let i = 0; i < res.length; i++) {
              resArr.push({ label: res[i].title, value: res[i].value });
            }
            this.setState(
              {
                treeData: res,
                depArr: resArr,
                deptInfo: resArr,
                resArr: resArr,
              },
              () => {
                this.query();
              }
            );
          });
        } else {
          this.query();
        }
      }
    );
  }
  //图表
  drawEchar = () => {
    const { dataInfo } = this.state;
    // 绘制图表 折线图
    var trend = echarts.init(document.getElementById("trend"));
    trend.setOption({
      title: { text: "" },
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "cross",
          label: {
            backgroundColor: "#6a7985",
          },
        },
      },
      xAxis: {
        type: "category",
        boundaryGap: false,
        // data: ["2019-01", "2019-02", "2019-03", "2019-04", "2019-05", "2019-06", "2019-07", "2019-08", "2019-09", "2019-10", "2019-11", "2019-12", "2020-01", "2020-02", "2020-03", "2020-04"],
        data: dataInfo.consumption.date,
      },
      yAxis: {
        type: "value",
        axisLine: {
          show: false,
        },
        axisLabel: {
          formatter: function (a) {
            return a / 1000 + "k";
          },
        },
      },
      series: [
        {
          name: "销量",
          type: "line",
          // data: [10, 20, 36, 10, 10, 20, 5, 20, 36, 10, 10, 20, 5, 20, 36, 10],
          data: dataInfo.consumption.value,
          itemStyle: {
            normal: {
              color: "#1890FF",
            },
          },
        },
      ],
    });
    //柱状图
    var depName = dataInfo.depConsume.depName;
    var OrderData = [];
    for (let i = 0; i < depName.length; i++) {
      let d = depName[i].split("/");
      OrderData.push(d[d.length - 1]);
    }
    console.log("OrderData", OrderData);
    var consume = echarts.init(document.getElementById("consume"));
    consume.setOption({
      tooltip: {
        trigger: "axis",
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
        },
      },
      xAxis: {
        type: "category",
        data: OrderData,
      },
      yAxis: {
        type: "value",
        axisLine: {
          show: false,
        },
        axisLabel: {
          formatter: function (a) {
            return a / 1000 + "k";
          },
        },
      },
      series: [
        {
          name: "消费金额",
          data: dataInfo.depConsume.OrderMoney,
          type: "bar",
          showBackground: false,
          barWidth: 20,
          itemStyle: {
            normal: {
              color: "#1890FF",
              width: "20px",
            },
          },
        },
      ],
    });
    let cirdata = dataInfo.expendDis;
    let cirInfo = [];
    for (let i = 0; i < cirdata.length; i++) {
      cirInfo.push({ name: cirdata[i].productName, value: cirdata[i].productNum });
    }
    //饼状图
    var expend = echarts.init(document.getElementById("expend"));
    expend.setOption({
      legend: {
        selected: false,
        icon: "circle",
        show: true,
        bottom: 10,
      },
      series: [
        {
          name: "费用支出明细",
          type: "pie",
          radius: ["30%", "50%"],
          avoidLabelOverlap: true,
          selectedMode: "single",
          legendHoverLink: false,
          hoverAnimation: false,
          label: {
            show: true,
            position: "outside",
            formatter: "{a|{b}:{d}%\n{c}}",
            rich: {
              a: {
                color: "#333333",
              },
            },
          },
          // emphasis: {
          //   label: {
          //     show: true,
          //     fontSize: "",
          //     fontWeight: "bold",
          //   },
          // },
          labelLine: {
            show: true,
          },

          color: ["#FBD438", "#FB7A38", "#993C3C", "#4ECC74", "#29A0A0", "#3BA1FF", "#007070", "#2B70AF"],
          data: cirInfo, //productName productNum
        },
      ],
    });

    expend.on("legendselectchanged", function (params) {
      return;
    });

    //横柱状图
    var city = echarts.init(document.getElementById("city"));
    city.setOption({
      tooltip: {
        trigger: "axis",
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
        },
      },
      xAxis: {
        type: "value",
        boundaryGap: [0, 0.01],
        axisLine: {
          show: false,
        },
      },
      yAxis: {
        type: "category",
        data: dataInfo.cityTop.cityName.reverse(),
      },
      series: [
        {
          name: "出行次数",
          type: "bar",
          data: dataInfo.cityTop.cityNum.reverse(),
          barWidth: 20,
          itemStyle: {
            normal: {
              color: "#1890FF",
            },
          },
        },
      ],
    });
  };
  //查询
  query = () => {
    const { dataType, timeType, depArr, tradeStartTime, tradeEndTime, payType, proType, corpNo, tmcNo, source, orderUserNo, deptInfo } = this.state;

    this.setState({
      isSenior: false,
    });
    var start = dataType === 1 ? tradeStartTime + " 00:00:00" : tradeStartTime + "-01 00:00:00";
    var end = dataType === 1 ? tradeEndTime + " 23:59:59" : tradeEndTime + "-01 23:59:59";
    var param = {};
    var depA = [];
    for (let i = 0; i < depArr.length; i++) {
      depA.push(depArr[i].label);
    }
    var depB = [];
    for (let i = 0; i < depA.length; i++) {
      let d = depA[i].split("/");
      depB.push(d[d.length - 1]);
    }
    if (source === "admin") {
      param = {
        statisticType: dataType,
        timeType: timeType,
        tradeStartTime: start,
        tradeEndTime: end,
        CorpNo: corpNo,
        tmcNO: tmcNo,
        payType: payType,
        productTypeName: proType,
        PassengerDepartmentName: depB,
      };
    } else {
      param = {
        statisticType: dataType,
        timeType: timeType,
        tradeStartTime: start,
        tradeEndTime: end,
        CorpNo: corpNo,
        tmcNO: tmcNo,
        payType: payType,
        productTypeName: proType,
        orderUserNo: orderUserNo,
      };
    }
    axios({
      method: "post",
      url: API.CONSUMER,
      headers: {
        "Content-Type": "application/json",
      },
      data: param,
    }).then((res) => {
      if (res.status === 200) {
        var obj = res.data.obj;
        this.setState(
          {
            dataInfo: obj,
          },
          () => {
            this.drawEchar();
          }
        );
      }
    });
  };
  //部门
  genTreeNode = (deptN0, pid, isLeaf = false, treeid, treename) => {
    const { tmcNo, corpNo } = this.state;
    return new Promise((resolve, rej) => {
      let treeArr = [];
      axios({
        method: "post",
        url: API.COMMON,
        headers: {
          "Content-Type": "application/json",
        },
        data: {
          companyNO: corpNo,
          companyName: "",
          tmcNO: tmcNo,
          deptNO: deptN0,
        },
      })
        .then((res) => {
          let data = res.data.content.outQueryDeptList;
          for (let i in data) {
            let item = data[i];
            treeArr.push({
              key: item.depNO,
              id: item.deptNO,
              pId: pid,
              value: item.deptNO,
              title: item.deptName,
              isLeaf: isLeaf,
              treeid: treeid ? treeid + "/" + item.deptNO : item.deptNO,
              treename: treename ? treename + "/" + item.deptName : item.deptName,
            });
          }
          resolve(treeArr);
        })
        .catch((res) => {
          console.log(111, res);
        });
    });
  };

  onLoadData = (treeNode) =>
    new Promise((resolve) => {
      const { treeData } = this.state;
      // var data = [];
      setTimeout(() => {
        this.genTreeNode(treeNode.value, treeNode.value, false, treeNode.treeid, treeNode.treename).then((res) => {
          if (res.length === 0) {
            treeNode.isLeaf = true;
            for (let i in treeData) {
              if (treeNode.value === treeData[i].value) {
                treeData[i].isLeaf = true;
              }
            }
          }
          this.setState({
            treeData: this.state.treeData.concat(res),
          });
        });

        resolve();
      }, 300);
    }).then(() => {});
  onSelect = (value, node, extra) => {
    // console.log("value", value);
    // this.setState({
    //   depArr: value,
    // });
    setTimeout(() => {
      const { deptInfo, depArr } = this.state;
      console.log("deptInfo", deptInfo);
      deptInfo[deptInfo.length - 1].label = node.treename;
      deptInfo[deptInfo.length - 1].value = node.treeid;
      depArr.push(deptInfo[deptInfo.length - 1]);
      console.log("depArr", depArr);
      this.setState({
        deptInfo,
        depArr: depArr,
      });
    }, 500);
  };
  //选择部门
  onChangeDept = (value, label, extra) => {
    const { depArr } = this.state;
    this.setState({
      deptInfo: value,
    });
    if (!extra.selected) {
      let arr = [];
      for (let i = 0; i < value.length; i++) {
        arr.push(value[i].value);
      }
      console.log("arr", arr, depArr);

      let TreeArr = [];
      for (let i = 0; i < depArr.length; i++) {
        var arrTreenode = depArr[i].value.split("/");
        if (arr.indexOf(arrTreenode[arrTreenode.length - 1]) > -1) {
          TreeArr.push(depArr[i]);
        }
      }
      console.log("TreeArr", TreeArr);
      this.setState({
        depArr: TreeArr,
      });
    }
  };

  //按日按月
  onChangeDataType = (e) => {
    let pick = "";
    var s1 = "";
    var e1 = "";
    if (e.target.value === 0) {
      pick = "month";
      var time = getLast3Month();
      s1 = time.last;
      e1 = time.now;
    } else {
      pick = "data";
      s1 = deStart; //开始时间
      e1 = deEnd; //结束时间
    }
    this.setState({
      dataType: e.target.value,
      pickType: pick,
      tradeStartTime: s1,
      tradeEndTime: e1,
    });
  };
  //改变时间类型
  onChangeTimeType = (value) => {
    this.setState({
      timeType: value,
    });
  };
  //改变日期
  onChangeData = (e, value) => {
    this.setState({
      tradeStartTime: value[0],
      tradeEndTime: value[1],
    });
  };

  //改变支付方式
  onChangePay = (e, value) => {
    this.setState({
      payType: e,
    });
  };

  //改变产品类型
  onChangePro = (data) => {
    let pType = [];
    let { proTypeValue } = this.state;
    if (data.value === 0) {
      data.active = !data.active;
      pType = [0];
      for (let i in proTypeValue) {
        if (proTypeValue[i].value !== 0) proTypeValue[i].active = false;
      }
    } else {
      data.active = !data.active;
      proTypeValue[0].active = false;
      let pArr = [];
      pArr = proTypeValue.filter((val) => {
        return val.active === true;
      });
      for (let i in pArr) {
        pType.push(pArr[i].value);
      }
      if (pType.length === 0 || pType.length === proTypeValue.length - 1) {
        proTypeValue[0].active = true;
        for (let i in proTypeValue) {
          if (proTypeValue[i].value !== 0) proTypeValue[i].active = false;
        }
        pType = ["1", "2", "3", "4", "5", "6"];
      }
    }
    this.setState({
      proTypeValue: proTypeValue,
      proType: pType,
    });
  };

  //移动端日期取消
  onCancel = () => {
    document.getElementsByTagName("body")[0].style.overflowY = this.originbodyScrollY;
    this.setState({
      mShow: false,
      tradeStartTime: undefined,
      tradeEndTime: undefined,
    });
  };
  //移动端日期选择
  onConfirm = (startTime, endTime) => {
    document.getElementsByTagName("body")[0].style.overflowY = this.originbodyScrollY;
    this.setState({
      mShow: false,
      tradeStartTime: transformTimestamp(startTime, "-"),
      tradeEndTime: transformTimestamp(endTime, "-"),
    });
  };
  onSelectHasDisableDate = (dates) => {
    console.warn("onSelectHasDisableDate", dates);
  };
  isShow = () => {
    this.setState({
      mShow: true,
    });
  };
  //移动端改变产品类型
  onmproChange = (value) => {
    if (value.length > 1 && value.indexOf("0") > -1) {
      value = value.filter((item) => {
        return item !== "0";
      });
    }
    this.setState({
      proType: value,
      productTypeName: value,
    });
  };

  //跳转选择部门
  goSelctDept = () => {
    const { redirect } = this.props;
    redirect("/selectDept", "right", true);
  };
  //高级筛选
  showAll = () => {
    this.setState({
      isSenior: true,
    });
  };

  //重置
  clear = () => {
    const { resArr } = this.state;
    this.setState({
      dataType: 1,
      timeType: "0",
      tradeStartTime: deStart, //开始时间
      tradeEndTime: deEnd, //结束时间
      payType: "0", //支付方式
      proType: ["0"], //产品类型
      deptInfo: resArr,
      depArr: resArr,
      pickType: "date", //时间选择器类型
      proTypeValue: [
        { title: "全部", value: 0, active: true },
        { title: "国内机票", value: 1, active: false },
        { title: "国际机票", value: 2, active: false },
        { title: "国内酒店", value: 3, active: false },
        { title: "国际酒店", value: 4, active: false },
        { title: "火车票", value: 5, active: false },
        { title: "用车", value: 6, active: false },
      ],
      deptInfo: resArr,
    });
  };
  onChangeMouth = (e) => {
    const { tradeStartTime, tradeEndTime } = this.state;
    var t = e.target.value;
    var s = "",
      e = "";
    if (t == 3) {
      var time = getLast3Month();
      s = time.last;
      e = time.now;
    } else if (t == 6) {
      var time = getLast6Month();
      console.log(time);
      s = time[time.length - 1];
      e = time[0];
    } else {
      var data = new Date();
      var year = data.getFullYear();
      var mon = data.getMonth() + 1 < 10 ? "0" + Number(data.getMonth() + 1) : data.getMonth() + 1;
      s = Number(year - 1) + "-" + mon;
      e = Number(year) + "-" + mon;
    }
    // var s = tradeEndTime.split('')
    // if(t == )
    this.setState({
      tradeStartTime: s,
      tradeEndTime: e,
    });
  };
  goback = () => {
    // var ua = navigator.userAgent.toLowerCase();
    var u = navigator.userAgent;
    var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1; //android终端
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    if (isiOS) {
      if (window && window.webkit && window.webkit.messageHandlers) {
        window.webkit.messageHandlers.getCityLocation.postMessage(null);
      }
    } else if (isAndroid) {
      window.WebApp.getCityLocation();
    }
    if (isiOS) {
      window.webkit.messageHandlers.back.postMessage(null);
    } else {
      window.WebApp.back();
    }
  };
  render() {
    const { dataType, timeType, payType, treeData, road, pickType, proTypeValue, dataInfo, isSenior, source, tradeStartTime, tradeEndTime } = this.state;
    return (
      <div className="page">
        <div className="content Index">
          <p className="title" style={{ background: (isSenior && road == "app") || road == "pc" ? "#fff" : "#f4f4f4" }}>
            <span className="right">
              <img alt="" onClick={this.goback} src={require("../../assets/images/right.png")} />
            </span>
            差旅分析
            <span className="right"></span>
          </p>
          <div className=" emroydiv" style={{ dispaly: road == "app" ? "block" : "none" }}></div>
          <div className="contentTop" style={{ background: (isSenior && road == "app") || road == "pc" ? "#fff" : "#f4f4f4" }}>
            <div className="btn_right">
              <Button>下载差旅分析</Button>
              <Button className="mr_10">查看消费报表</Button>
            </div>
            <p className="min_title">查询筛选</p>
            <div className={isSenior ? "fromList showList" : "fromList hideList"}>
              <div>
                <Radio.Group onChange={this.onChangeDataType} value={dataType}>
                  <Radio value={1}>按日统计</Radio>
                  <Radio value={0}>按月统计</Radio>
                </Radio.Group>
              </div>
              <Button className="screenBtn" onClick={this.showAll}>
                高级筛选
              </Button>
              <div className="timediv">
                <Input.Group compact>
                  <Select defaultValue={timeType} className="timeStyle" onChange={this.onChangeTimeType}>
                    <Option value="0">交易时间</Option>
                    <Option value="1">出行时间</Option>
                    <Option value="2">到达时间</Option>
                  </Select>
                  <div className="showm pickcan" style={{ display: road == "app" && dataType === 0 ? "inline-flex" : "none" }}>
                    <Radio.Group onChange={this.onChangeMouth} defaultValue="3">
                      <Radio.Button value="3">近三个月</Radio.Button>
                      <Radio.Button value="6">近六个月</Radio.Button>
                      <Radio.Button value="12">近一年</Radio.Button>
                    </Radio.Group>
                  </div>
                  <div className="showm pickcan" onClick={this.isShow} style={{ display: road == "app" && dataType === 1 ? "inline-flex" : "none" }}>
                    <Input value={this.state.tradeStartTime} placeholder="开始时间" />
                    <Input value={this.state.tradeEndTime} placeholder="结束时间" />
                  </div>
                  <RangePicker
                    className="RangePick showpc"
                    locale={locale}
                    picker={pickType}
                    onChange={this.onChangeData}
                    value={[moment(tradeStartTime, dateFormat), moment(tradeEndTime, dateFormat)]}
                    defaultValue={[moment(defultStart, defultFormat), moment(teaday, dateFormat)]}
                    dateRender={(current) => {
                      const style = {};
                      if (current.date() === 1) {
                        style.border = "1px solid #1890ff";
                        style.borderRadius = "50%";
                      }
                      return (
                        <div className="ant-picker-cell-inner" style={style}>
                          {current.date()}
                        </div>
                      );
                    }}
                  />
                </Input.Group>
              </div>
              <div className="mproType">
                <p>产品类型</p>
                <TreeSelect
                  showSearch
                  style={{ width: "100%" }}
                  value={this.state.proType}
                  dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                  placeholder="请选择产品类型"
                  allowClear
                  multiple
                  treeDefaultExpandAll
                  onChange={this.onmproChange}
                >
                  <TreeNode value="0" title="全部">
                    <TreeNode value="1" title="国内机票"></TreeNode>
                    <TreeNode value="2" title="国际机票"></TreeNode>
                    <TreeNode value="3" title="国内酒店"></TreeNode>
                    <TreeNode value="4" title="国际酒店"></TreeNode>
                    <TreeNode value="5" title="火车票"></TreeNode>
                    <TreeNode value="6" title="用车"></TreeNode>
                  </TreeNode>
                </TreeSelect>
              </div>
              <div className="flexBox payType">
                <p>支付方式</p>
                <Select value={payType} className="selectPay" onChange={this.onChangePay}>
                  <Option value="0">全部</Option>
                  <Option value="1">月结</Option>
                  <Option value="2">现付</Option>
                  <Option value="3">预存</Option>
                  <Option value="4">线下</Option>
                </Select>
              </div>
              {/* <div className="mdep">
                <p>部门名称</p>
                <span onClick={this.goSelctDept}>
                  当前选择<span style={{ color: "rgba(31, 56, 88, 0.3)" }}>（&nbsp;</span>6 <span style={{ color: "rgba(31, 56, 88, 0.3)" }}>）</span>{" "}
                </span>
                  </div>*/}

              <div className=" flexBox proType">
                <p>产品类型</p>
                <ul className="proUL ">
                  {proTypeValue.map((item, index) => {
                    return (
                      <li key={index} onClick={this.onChangePro.bind(this, item)} className={item.active ? "proLI active" : "proLI"} value={item.value}>
                        {item.title}
                      </li>
                    );
                  })}
                </ul>
              </div>

              <div className="flexBox depName" style={{ display: source == "admin" ? "block" : "none" }}>
                <p>部门名称</p>
                <TreeSelect
                  treeDataSimpleMode
                  multiple
                  className="depdiv"
                  value={this.state.deptInfo}
                  dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                  placeholder="请选择部门"
                  onSelect={this.onSelect}
                  loadData={this.onLoadData}
                  treeData={treeData}
                  onChange={this.onChangeDept}
                  labelInValue
                />
              </div>
              <div className="btn" style={{ display: (road == "app" && isSenior) || road == "pc" ? "flex" : "none" }}>
                <Button className="mr_10" onClick={this.clear}>
                  重置
                </Button>
                <Button onClick={this.query} className="mr_10" type="primary" danger>
                  查询
                </Button>
              </div>
            </div>
            <div className="midden" style={{ display: isSenior && road == "app" ? "none" : "block" }}>
              <ul className="flexInfo">
                <li className="red">
                  <div className="imgbox">
                    <img alt="" src={require("../../assets/images/dingdan.png")} />
                  </div>
                  <div className="infobox">
                    <p className="all">
                      全部订单:<span>{format(dataInfo.allOrder)}单</span>
                    </p>
                    <p className="yi">{format(dataInfo.order)}单</p>
                    <p className="title1">订单量</p>
                  </div>
                </li>
                <li className="orange">
                  <div className="imgbox">
                    <img alt="" src={require("../../assets/images/renshu.png")} />
                  </div>
                  <div className="infobox">
                    <p className="all">
                      全部消费人次:<span>{format(dataInfo.allConsume)}人</span>
                    </p>
                    <p className="yi">{format(dataInfo.consume)}人</p>
                    <p className="title1">消费人次</p>
                  </div>
                </li>
                <li className="blue">
                  <div className="imgbox">
                    <img alt="" src={require("../../assets/images/jine.png")} />
                  </div>
                  <div className="infobox">
                    <p className="all">
                      总消费金额:<span>{format(dataInfo.allAmount)}元</span>
                    </p>
                    <p className="yi">{format(dataInfo.amount)}元</p>
                    <p className="title1">消费金额</p>
                  </div>
                </li>
                <li className="grad">
                  <div className="imgbox">
                    <img alt="" src={require("../../assets/images/renjun.png")} />
                  </div>
                  <div className="infobox">
                    <p className="all">
                      总人均消费:<span>{format(dataInfo.allAverage)}元</span>
                    </p>
                    <p className="yi">{format(dataInfo.average)}元</p>
                    <p className="title1">平均人均消费</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div className="contentBottom" style={{ display: isSenior && road == "app" ? "none" : "block" }}>
            <div className="echart trend">
              <p className="echartsTitle"> 消费趋势</p>
              <div className="echartBox">
                <div id="trend" style={{ width: "100%", height: "100%" }}></div>
              </div>
            </div>
            <div className="echart" style={{ display: source == "admin" ? "flex" : "none" }}>
              <p className="echartsTitle"> 按部门统计消费金额</p>
              <div className="echartBox">
                <div id="consume" style={{ width: "100%", height: "100%" }}></div>
              </div>
            </div>
            <div className="echartCon">
              <div className="echart">
                <p className="echartsTitle"> 费用支出分布</p>

                <div className="echartBox">
                  <p className="payP">
                    <span className="paycum">支出总额</span> <span className="paynum">{format(dataInfo.allAmount)}</span>
                  </p>
                  <div id="expend" style={{ width: "100%", height: "85%" }}></div>
                </div>
              </div>
              <div className="echart">
                <p className="echartsTitle"> 出行热门城市</p>
                <div className="echartBox">
                  <div id="city" style={{ width: "100%", height: "100%" }}></div>
                </div>
              </div>
            </div>
          </div>

          <Calendar
            {...this.state.config}
            visible={this.state.mShow}
            onCancel={this.onCancel}
            onConfirm={this.onConfirm}
            onSelectHasDisableDate={this.onSelectHasDisableDate}
            getDateExtra={this.getDateExtra}
            defaultTimeValue={[tradeStartTime, tradeEndTime]}
            minDate={new Date(+now - 31536000000)}
            maxDate={new Date(+now + 31536000000)}
            infiniteOpt={true}
            initalMonths={36}
          />
        </div>
      </div>
    );
  }
}
Index = Page(Index);

export default withRouter(Index);
