import React from "react";

const Loading = () => (
  <div className="loading">
    <div className="loadImg">
      {/* <img src="/images/newloading.gif"/> */}
      <span>努力加载中...</span>
    </div>
  </div>
);

export default Loading;
