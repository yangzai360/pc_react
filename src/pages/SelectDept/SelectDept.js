import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { NavBar, Icon, List, Checkbox, Button } from "antd-mobile";
import { Breadcrumb } from "antd";
import Page from "../../components/Page/Page";
import { getDept } from "../../redux/actions/echarts";

import "./SelectDept.scss";
const CheckboxItem = Checkbox.CheckboxItem;
console.log("connect", connect);
class SelectDept extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    // const { getDept } = this.props;
    // console.log(getDept);
    // getDept("20190117170119359110", "20190117170119359110");
  }
  //返回
  goback = () => {
    const { redirect } = this.props;
    redirect("/", "right", true);
  };

  //选择部门

  changeSelectDept = () => {};

  render() {
    const data = [
      { value: 0, label: "Ph.D." },
      { value: 1, label: "Bachelor" },
      { value: 2, label: "College diploma" }
    ];
    return (
      <div className="page SelectDept">
        <NavBar mode="light" icon={<Icon type="left" />} onLeftClick={this.goback}>
          差旅分析
        </NavBar>
        <div className="content">
          <div className="bread">
            <Breadcrumb className="breadcrumb" separator=">">
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item href="">Application Center</Breadcrumb.Item>
              <Breadcrumb.Item href="">Application List</Breadcrumb.Item>
              <Breadcrumb.Item>An Application</Breadcrumb.Item>
            </Breadcrumb>
          </div>
          <div className="allSelect">
            <CheckboxItem>全选</CheckboxItem>
          </div>
          <List className="listSelect">
            {data.map(i => (
              <CheckboxItem key={i.value} onChange={() => this.changeSelectDept(i.value)}>
                {i.label}
                <div className="down">下级</div>
              </CheckboxItem>
            ))}
          </List>
        </div>
        <div className="fixBottom">
          <div className="fixbtn">
            <div className="fixword">
              当前选择<span style={{ color: "rgba(31, 56, 88, 0.3)" }}>（</span> 6 <span style={{ color: "rgba(31, 56, 88, 0.3)" }}>）</span>
            </div>
            <div>
              <Button className="btn" type="warning">
                确定
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("state", state);
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getDept: bindActionCreators(getDept, dispatch)
  };
};

SelectDept = connect(mapStateToProps, mapDispatchToProps)(SelectDept);
SelectDept = Page(SelectDept);
export default withRouter(SelectDept);
