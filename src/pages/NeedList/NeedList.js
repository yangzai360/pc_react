import React from "react";
import Page from "../../components/Page/Page";
import { withRouter } from "react-router-dom";
import { hasParameter, transformTimestamp, _fetch, format } from "../../utils/util";
import { NavBar, Icon, List, InputItem, WhiteSpace, TextareaItem, Button, Calendar, Steps } from "antd-mobile";
import { Radio, Select, Badge, Input, Modal } from "antd";
import "./NeedList.scss";
import { createForm } from "rc-form";
import zhCN from "antd-mobile/lib/calendar/locale/zh_CN";
import API from "../../constants/api";
import { DownOutlined, CheckCircleFilled, UpOutlined } from "@ant-design/icons";

import { pageInputScroll } from "../../utils/pageInputScroll";

const { Option } = Select;
const Step = Steps.Step;
const extra = {};
const now = new Date();
for (let i = 0; i < now.getDate(); i++) {
  extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() - i * 1)] = { info: "", disable: true };
}
// extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 5)] = { info: "Disable", disable: true };
// extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 6)] = { info: "Disable", disable: true };
// extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7)] = { info: "Disable", disable: true };
// extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8)] = { info: "Disable", disable: true };

Object.keys(extra).forEach((key) => {
  const info = extra[key];
  const date = new Date(key);
  if (!Number.isNaN(+date) && !extra[+date]) {
    extra[+date] = info;
  }
});
// var ua = navigator.userAgent.toLowerCase();
var u = navigator.userAgent;
var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1; //android终端
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
if (isiOS) {
  if (window && window.webkit && window.webkit.messageHandlers) {
    window.webkit.messageHandlers.getCityLocation.postMessage(null);
  }
} else if (isAndroid) {
  window.WebApp.getCityLocation();
}

class NeedList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cropId: "", //公司Id
      cropName: "中国石油化工集团公司物资装部（国际事业公司）",
      depId: "",
      depName: "",
      userId: "",
      userName: "",
      userPhone: "",
      userEmail: "",
      config: {
        locale: zhCN,
      },
      mShow: false,
      startBeginTime: "", //时间范围起
      startTimeC: "",
      endTimeC: "",
      startEndTime: "", //时间范围止
      customType: "1", //定制类型
      subjectData: [], //所有主题
      contactType: "1", //联系方式
      contactTypeName: "微信",
      customThemeId: "", //定制主题id
      customThemeName: "", //定制主题名称
      showList: false,
      orderId: "",
      isReply: false, //回复框
      replyCon: "",
      replyMsg: "", //回复内容
      replyTime: "", //回复时间
      statusName: "",
      status: 1, //回复状态
      isOpen: false, //回复是否展开
      replyUsername: "", //定制师名字
      errorArr: [],
      isReadFlag: false, //是否已读
      isReplyShow: false, //判读回复是否显示
      source: "app",
    };
  }
  componentDidMount() {
    pageInputScroll();
    var orderId = hasParameter("orderId", this.props);
    var isread = hasParameter("isread", this.props);
    var userId = hasParameter("userId", this.props);
    var source = hasParameter("source", this.props); //来源  pc app
    var userName = hasParameter("userName", this.props);
    if (orderId) {
      this.setState({
        orderId: orderId,
        userId: userId,
        showList: true,
        userName: userName,
        source: source,
      });
      if (!isread) {
        var readparam = {
          submitUserId: userId,
          demandOrderId: orderId,
        };
        if (isiOS) {
          if (window && window.webkit && window.webkit.messageHandlers) {
            window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.SIGNREAD, data: readparam }));
          }
        } else if (isAndroid) {
          window.WebApp.getNativeParams(API.SIGNREAD, JSON.stringify(readparam));
        }

        setTimeout(() => {
          _fetch(API.SIGNREAD, "post", {
            bundleIdentifier: "com.epectravel.epec.trip",
            clienttype: 1,
            data: readparam,
            mcode: "ANDROID_I869019038625257",
            method: API.SIGNREAD,
            pid: "20180625142349344452",
            requestid: "1586258163002",
            timestamp: "2020-04-07 19:16:03",
            token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
            version: "7440",
            sign: "E82B2B538298BC0D01C042827433AE54",
            mCode: "ANDROID_I869019038625257",
          }).then((res) => {});
        }, 500);
      }
      if (isiOS) {
        if (window && window.webkit && window.webkit.messageHandlers) {
          window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.FINDBYORDERID, data: { demandOrderId: orderId } }));
        }
      } else if (isAndroid) {
        window.WebApp.getNativeParams(API.FINDBYORDERID, JSON.stringify({ demandOrderId: orderId }));
      }
      setTimeout(() => {
        _fetch(API.FINDBYORDERID, "post", {
          bundleIdentifier: "com.epectravel.epec.trip",
          clienttype: 1,
          data: {
            demandOrderId: orderId,
          },
          mcode: "ANDROID_I869019038625257",
          method: API.FINDBYORDERID,
          pid: "20180625142349344452",
          requestid: "1586258163002",
          timestamp: "2020-04-07 19:16:03",
          token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
          version: "7440",
          sign: "E82B2B538298BC0D01C042827433AE54",
          mCode: "ANDROID_I869019038625257",
        }).then((res) => {
          var needData = res.data.content;
          var open = false;
          if (needData.customUserId !== "" && needData.customUserId === userId) {
            open = true;
          }
          this.props.form.setFieldsValue({
            corpName: needData.corpName,
            startAddress: needData.startAddress,
            phonenumber: needData.phoneNumber,
            email: needData.email,
            contact: needData.contact,
            arriveAddress: needData.arriveAddress,
            startTime: transformTimestamp(needData.startBeginTime) + "-" + transformTimestamp(needData.startEndTime),
            demandDesc: needData.demandDesc,
            contactAccount: needData.contactAccount,
            personCount: needData.personCount,
            budget: format(needData.budget),
          });
          this.setState({
            customType: needData.customType,
            contactType: needData.contactType,
            subjectData: [{ customThemeName: needData.customThemeName, active: true }],
            replyMsg: needData.replyMsg,
            replyTime: transformTimestamp(needData.replyTime),
            statusName: needData.statusName,
            status: needData.status,
            replyUsername: needData.replyUsername,
            isReplyShow: open,
          });
        });
      }, 1000);
    } else {
      localStorage.setItem("userId", hasParameter("userId", this.props));
      this.setState({
        cropId: hasParameter("cropId", this.props),
        cropName: hasParameter("cropName", this.props),
        depId: hasParameter("depId", this.props),
        depName: hasParameter("depName", this.props),
        userId: hasParameter("userId", this.props),
        userName: hasParameter("userName", this.props),
        userPhone: hasParameter("userPhone", this.props),
        userEmail: hasParameter("userEmail", this.props),
        showList: false,
      });
      this.props.form.setFieldsValue({
        corpName: hasParameter("cropName", this.props),
        contact: hasParameter("userName", this.props),
        phonenumber: hasParameter("userPhone", this.props),
        email: hasParameter("userEmail", this.props),
        startAddress: localStorage.getItem("dealCityLocation"),
      });

      //获取已读未读标记
      if (isiOS) {
        if (window && window.webkit && window.webkit.messageHandlers) {
          window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.READFLAG, data: { submitUserId: userId } }));
        }
      } else if (isAndroid) {
        window.WebApp.getNativeParams(API.READFLAG, JSON.stringify({ submitUserId: userId }));
      }
      setTimeout(() => {
        _fetch(API.READFLAG, "post", {
          bundleIdentifier: "com.epectravel.epec.trip",
          clienttype: 1,
          data: {
            submitUserId: userId,
          },
          mcode: "ANDROID_I869019038625257",
          method: API.READFLAG,
          pid: "20180625142349344452",
          requestid: "1586258163002",
          timestamp: "2020-04-07 19:16:03",
          token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
          version: "7440",
          sign: "E82B2B538298BC0D01C042827433AE54",
          mCode: "ANDROID_I869019038625257",
        }).then((res) => {
          var data = res.content;
          this.setState({
            isReadFlag: data,
          });
        });
      }, 500);
      //定制主题
      if (isiOS) {
        if (window && window.webkit && window.webkit.messageHandlers) {
          window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: "listCustomTheme", data: { id: "1" } }));
        }
      } else if (isAndroid) {
        window.WebApp.getNativeParams("listCustomTheme", JSON.stringify({}));
      }

      setTimeout(() => {
        _fetch(API.CUSTOMTHEME, "post", {
          bundleIdentifier: "com.epectravel.epec.trip",
          clienttype: 1,
          data: {},
          mcode: "ANDROID_I869019038625257",
          method: "listCustomTheme",
          pid: "20180625142349344452",
          requestid: "1586258163002",
          timestamp: "2020-04-07 19:16:03",
          token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
          version: "7440",
          sign: "E82B2B538298BC0D01C042827433AE54",
          mCode: "ANDROID_I869019038625257",
        }).then((res) => {
          let data = res.data.content;
          for (let i in data) {
            data[i].active = false;
          }
          this.setState({
            subjectData: data,
          });
        });
      }, 500);
    }
  }
  handleChange = (value, data) => {
    this.setState({
      contactType: value,
      contactTypeName: data.children,
    });
    this.props.form.setFieldsValue({
      contactAccount: "",
    });
  };

  isShow = () => {
    this.setState({
      mShow: true,
    });
  };

  //日期取消
  onCancel = () => {
    document.getElementsByTagName("body")[0].style.overflowY = this.originbodyScrollY;
    this.setState({
      mShow: false,
    });
  };
  //日期确认
  onConfirm = (startTime, endTime) => {
    document.getElementsByTagName("body")[0].style.overflowY = this.originbodyScrollY;
    this.setState({
      mShow: false,
      startBeginTime: transformTimestamp(startTime),
      startEndTime: transformTimestamp(endTime),
      startTimeC: transformTimestamp(startTime, "-"),
      endTimeC: transformTimestamp(endTime, "-"),
    });
    this.props.form.setFieldsValue({
      startTime: transformTimestamp(startTime) + "——" + transformTimestamp(endTime),
    });
  };
  onSelectHasDisableDate = (dates) => {
    console.warn("onSelectHasDisableDate", dates);
  };
  //键盘遮挡
  topScrollInput = (obj) => {
    // var target = obj;
    // //使用定时器让输入框上滑时更加自然
    // setTimeout(function () {
    //   target.scrollIntoView(true);
    // }, 100);
  };
  //公司类型切换
  cropType = (e) => {
    const { showList } = this.state;
    if (showList) {
      return;
    } else {
      this.setState({
        customType: e.target.value,
      });
    }
  };
  //改变定制主题
  onChangeSub = (data) => {
    const { subjectData } = this.state;
    //多选
    // data.active = !data.active;
    // var arr = [];
    // arr = subjectData.filter(item => {
    //   return item.active === true;
    // });
    // this.setState({
    //   subjectData: subjectData
    // });
    for (let i in subjectData) {
      subjectData[i].active = false;
    }
    data.active = true;
    this.setState({
      customThemeId: data.customThemeId,
      customThemeName: data.customThemeName,
    });
  };

  //提交
  submit = () => {
    const { redirect } = this.props;

    const { cropId, customType, userName, userId, startTimeC, endTimeC, contactType, contactTypeName, customThemeId, customThemeName } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var param = {
          corpNo: cropId,
          corpName: values.corpName,
          customType: customType,
          customTypeName: customType === "1" ? "公司" : "非公司",
          submitUsername: userName,
          submitUserId: userId,
          startAddress: values.startAddress,
          arriveAddress: values.arriveAddress,
          startBeginTime: startTimeC,
          startEndTime: endTimeC,
          personCount: values.personCount,
          budget: values.budget,
          contact: values.contact,
          phoneNumber: values.phonenumber,
          email: values.email,
          contactType: contactType,
          contactTypeName: contactTypeName,
          contactAccount: values.contactAccount,
          customThemeId: customThemeId,
          customThemeName: customThemeName,
          demandDesc: values.demandDesc,
        };
        //提交
        if (isiOS) {
          if (window && window.webkit && window.webkit.messageHandlers) {
            window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.ADDDEMAND, data: param }));
          }
        } else if (isAndroid) {
          window.WebApp.getNativeParams(API.ADDDEMAND, JSON.stringify(param));
        }
        setTimeout(() => {
          _fetch(API.ADDDEMAND, "post", {
            bundleIdentifier: "com.epectravel.epec.trip",
            clienttype: 1,
            data: param,
            mcode: "ANDROID_I869019038625257",
            method: API.ADDDEMAND,
            pid: "20180625142349344452",
            requestid: "1586258163002",
            timestamp: "2020-04-07 19:16:03",
            token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
            version: "7440",
            sign: "E82B2B538298BC0D01C042827433AE54",
            mCode: "ANDROID_I869019038625257",
          }).then((res) => {
            if (res.data.code === 1) {
              window.localStorage.setItem(API.FINDBYORDERID, "");
              redirect("/listInfo?orderId=" + res.data.content.demandOrderId + "&isread=" + "false" + "&userId=" + userId + "&source=" + "app", "right", false);
            }
          });
        }, 500);
      } else {
        let error = [];
        for (let key in err) {
          error.push(key);
        }
        this.setState({
          errorArr: error,
        });
      }
    });
  };
  //点击回复
  replyBtn = () => {
    this.setState({
      isReply: true,
    });
  };
  //确认回复
  replyOk = () => {
    const { userId, orderId, userName } = this.state;
    //回复
    this.props.form.validateFields((err, values) => {
      var param = {
        replyUserId: userId,
        demandOrderId: orderId,
        replyUsername: userName,
        replyMsg: values.replyCon,
      };
      if (isiOS) {
        if (window && window.webkit && window.webkit.messageHandlers) {
          window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.REPLY, data: param }));
        }
      } else if (isAndroid) {
        window.WebApp.getNativeParams(API.REPLY, JSON.stringify(param));
      }
      setTimeout(() => {
        _fetch(API.REPLY, "post", {
          bundleIdentifier: "com.epectravel.epec.trip",
          clienttype: 1,
          data: param,
          mcode: "ANDROID_I869019038625257",
          method: API.REPLY,
          pid: "20180625142349344452",
          requestid: "1586258163002",
          timestamp: "2020-04-07 19:16:03",
          token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
          version: "7440",
          sign: "E82B2B538298BC0D01C042827433AE54",
          mCode: "ANDROID_I869019038625257",
        }).then((res) => {
          if (res.data.code === 1) {
            this.setState({
              isReply: false,
            });
          }
        });
      }, 500);
    });
  };
  //取消回复
  replyCancel = () => {
    this.setState({
      isReply: false,
    });
  };
  //改变默认回复
  changeReply = (value, option) => {
    this.props.form.setFieldsValue({
      replyCon: option.children,
    });
  };
  //打开
  openShow = () => {
    this.setState({
      isOpen: true,
    });
  };
  //关闭
  cloneShow = () => {
    this.setState({
      isOpen: false,
    });
  };
  goback = () => {
    const { showList } = this.state;
    if (showList) {
      window.history.back();
    } else {
      if (isiOS) {
        window.webkit.messageHandlers.back.postMessage(null);
      } else {
        window.WebApp.back();
      }
    }
  };
  goMyList = () => {
    const { userId } = this.state;
    const { redirect } = this.props;

    redirect("/myList?userId=" + userId + "&source=list", "right", false);
  };
  changeReplyCont = (value, option) => {
    this.props.form.setFieldsValue({
      replyCon: option.children,
    });
  };
  getDateExtra = (date) => extra[+date];
  render() {
    const { getFieldProps } = this.props.form;
    const { customType, subjectData, contactType, showList, orderId, replyMsg, replyTime, statusName, status, isOpen, replyUsername, errorArr, isReadFlag, isReplyShow, source } = this.state;
    var listData = subjectData || [];
    return (
      <div className="page NeedList">
        <NavBar style={{ display: showList ? "none" : "flex" }} className="navbar trannav titledown" mode="light" icon={<Icon style={{ color: "#1b1b1b" }} type="left" />} onLeftClick={this.goback}>
          需求单
        </NavBar>

        <NavBar
          style={{ display: showList ? "flex" : "none" }}
          className="navbar trannav titledown redHeader"
          mode="light"
          icon={<Icon style={{ color: "#fff" }} type="left" />}
          onLeftClick={this.goback}
        >
          我的需求单
        </NavBar>

        <div className="content">
          {/* <div className="cropInfo">
            <p className="cropname">{cropName}</p>
            <p className="iconimg">
              <img alt="" src={require("../../assets/images/qie.png")} />
              切换
            </p>
            <div className="cropimg">
              <img alt="" src={require("../../assets/images/cropbg.png")} />
            </div>
    </div>*/}
          <div className="replyTip" style={{ display: showList ? "block" : "none" }}>
            <Icon type="check-circle-o" style={{ marginRight: "5px" }} />
            {statusName}
          </div>

          <div className="needInfo" style={{ display: showList ? "block" : "none" }}>
            <div className="info">
              <div className="title">
                <p>需求单号：{orderId}</p> <DownOutlined style={{ display: isOpen ? "none" : "block" }} onClick={this.openShow} />
              </div>
              <div className="replyInfo" style={{ display: isOpen ? "none" : "flex" }}>
                <div className="left">
                  <img src={require("../../assets/images/ld.png")} alt="" />
                </div>
                <div className="right">
                  <p style={{ display: status === 1 ? "none" : "block" }} className="replyconTip">
                    定制师{replyUsername}
                  </p>
                  <p className="replyContent">{replyMsg}</p>
                  <p className="replyconTip">{replyTime}</p>
                </div>
              </div>
              <div className="stepInfo" style={{ display: isOpen ? "block" : "none", position: "relative" }}>
                <Steps size="small" current={1}>
                  <Step icon={<CheckCircleFilled style={{ fontSize: "14px", color: "#DA312E" }} />} title={status === 1 ? replyMsg : "提交需求单成功1"} description={status === 1 ? replyTime : ""} />
                  <Step
                    style={{ display: status === 1 ? "block" : "none" }}
                    icon={<div style={{ background: "#E8EBEE", width: "14px", height: "14px", borderRadius: "50%", margin: "2px" }}></div>}
                    title="沟通并反馈结果"
                  />
                </Steps>
                <div className="replyInfo" style={{ display: status === 1 ? "none" : "flex" }}>
                  <div className="left" style={{ paddingLeft: "2px" }}>
                    <CheckCircleFilled style={{ fontSize: "14px", color: "#DA312E" }} />
                  </div>
                  <div className="right">
                    <p className="replyconTip">定制师{replyUsername}</p>
                    <p className="replyContent">{replyMsg}</p>
                    <p className="replyconTip">{replyTime}</p>
                  </div>
                </div>
                <UpOutlined onClick={this.cloneShow} style={{ position: "absolute", right: "10px", bottom: "10px" }} />
              </div>
            </div>
          </div>

          <div className="fromdiv">
            <WhiteSpace />
            <List className="formList">
              <div className="dingType">
                <p>定制类型</p>

                <Radio.Group size="large" value={customType}>
                  <Radio.Button value={customType} style={{ display: showList ? "block" : "none" }}>
                    {customType === "1" ? "公司" : "非公司"}
                  </Radio.Button>
                </Radio.Group>

                <Radio.Group onChange={this.cropType} style={{ display: showList ? "none" : "flex" }} value={customType} size="large">
                  <Radio.Button value="1">公司</Radio.Button>
                  <Radio.Button value="0">非公司</Radio.Button>
                </Radio.Group>
              </div>
              <InputItem className={customType === "1" ? "" : "isShow"} {...getFieldProps("corpName")} placeholder="" disabled editable={false} ref={(el) => (this.inputRef = el)}>
                公司名称
              </InputItem>
              <InputItem
                {...getFieldProps("startAddress", {
                  rules: [{ required: true, message: "预计人数不能为空" }],
                })}
                editable={!showList}
                placeholder="（必填）请输入"
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("startAddress") ? "err-list-item" : ""
                }
                ref={(el) => (this.inputRef = el)}
                extra={
                  showList ? (
                    ""
                  ) : (
                    <Icon
                      type="cross-circle-o"
                      color="#ccc"
                      onClick={() => {
                        this.props.form.setFieldsValue({
                          startAddress: "",
                        });
                      }}
                    />
                  )
                }
              >
                出发地
              </InputItem>
              <InputItem {...getFieldProps("arriveAddress")} editable={!showList} placeholder="（选填）请输入例如北京、上海" clear ref={(el) => (this.inputRef = el)}>
                目的地
              </InputItem>
              <InputItem
                editable={!showList}
                onClick={showList ? "" : this.isShow}
                {...getFieldProps("startTime", {
                  rules: [{ required: true, message: "出发日期不能为空" }],
                })}
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("startTime") ? "err-list-item nocale" : "nocale"
                }
                clear
                placeholder="（必填）请选择"
              >
                出发日期
              </InputItem>
              <InputItem
                type="number"
                editable={!showList}
                {...getFieldProps("personCount", {
                  rules: [{ required: true, message: "预计人数不能为空" }],
                })}
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("personCount") ? "err-list-item" : ""
                }
                placeholder="（必填）请输入"
                ref={(el) => (this.inputRef = el)}
                extra="人"
                clear
              >
                预计人数
              </InputItem>
              <InputItem
                type="number"
                editable={!showList}
                className="noborder"
                {...getFieldProps("budget", {
                  rules: [
                    { required: true, message: "预算金额不能为空" },
                    { required: /^[0-9]*[1-9][0-9]*$/, message: "请输入正整数" },
                  ],
                })}
                placeholder="（必填）请输入"
                className={errorArr.includes("budget") ? "err-list-item" : ""}
                ref={(el) => (this.inputRef = el)}
                extra="元"
                clear
              >
                预算金额
              </InputItem>
            </List>

            <WhiteSpace />
            <List className="formList">
              <InputItem
                editable={!showList}
                {...getFieldProps("contact", {
                  rules: [{ required: true, message: "联系人不能为空" }],
                })}
                placeholder="（必填）请输入"
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("contact") ? "err-list-item" : ""
                }
                ref={(el) => (this.inputRef = el)}
                extra={
                  showList ? (
                    ""
                  ) : (
                    <Icon
                      type="cross-circle-o"
                      onClick={() => {
                        this.props.form.setFieldsValue({
                          contact: "",
                        });
                      }}
                      color="#ccc"
                    />
                  )
                }
              >
                联系人
              </InputItem>
              <InputItem
                {...getFieldProps("phonenumber", {
                  rules: [{ required: true, message: "手机号不能为空" }],
                })}
                placeholder="（必填）请输入"
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("phonenumber") ? "err-list-item" : ""
                }
                editable={!showList}
                ref={(el) => (this.inputRef = el)}
                extra={
                  showList ? (
                    ""
                  ) : (
                    <Icon
                      type="cross-circle-o"
                      color="#ccc"
                      onClick={() => {
                        this.props.form.setFieldsValue({
                          phonenumber: "",
                        });
                      }}
                    />
                  )
                }
              >
                手机号码
              </InputItem>
              <InputItem
                {...getFieldProps("email", {
                  rules: [{ required: true, message: "公司邮箱不能为空" }],
                })}
                placeholder="（必填）请输入"
                editable={!showList}
                className={
                  // 如果errorArr里包含当前的id那么就给他颜色变红
                  errorArr.includes("email") ? "err-list-item" : ""
                }
                ref={(el) => (this.inputRef = el)}
                extra={
                  showList ? (
                    ""
                  ) : (
                    <Icon
                      type="cross-circle-o"
                      color="#ccc"
                      onClick={() => {
                        this.props.form.setFieldsValue({
                          email: "",
                        });
                      }}
                    />
                  )
                }
              >
                公司邮箱
              </InputItem>
              <InputItem className="noborder" clear {...getFieldProps("contactAccount")} placeholder="（选填）请输入" ref={(el) => (this.inputRef = el)}>
                <Select defaultValue={contactType} style={{ width: 80, border: "none" }} onChange={this.handleChange}>
                  <Option value="1">微信</Option>
                  <Option value="2">钉钉</Option>
                  <Option value="3">其他</Option>
                </Select>
              </InputItem>
            </List>
            <WhiteSpace />

            <List className="formList listItem">
              <p className="title">定制主题</p>
              <ul className="subject" style={{ display: listData.length > 0 ? (listData[0].customThemeName !== "" ? "flex" : "none") : "none" }}>
                {listData.map((item, index) => {
                  return (
                    <li key={index} onClick={this.onChangeSub.bind(this, item)} className={item.active ? "active" : ""} value={item.customThemeId}>
                      {item.customThemeName}
                    </li>
                  );
                })}
              </ul>
            </List>

            <WhiteSpace />

            <List style={{ marginBottom: isReplyShow || !showList ? "82px" : "10px" }} className="formList listItem">
              <p className={showList ? "borBot title" : "title"}>需求描述</p>
              <TextareaItem
                editable={!showList}
                {...getFieldProps("demandDesc")}
                rows={showList ? "" : 3}
                count={showList ? "" : 300}
                style={{ height: "90px" }}
                onClick={this.topScrollInput.bind(this)}
                placeholder="（选填）您可以在此输入个性化需求，如:同行人中是否含婴儿、老人，希望定制师何时联系您，对酒店、景点、出行方式的具体要求等。"
              />
            </List>
          </div>
          <div style={{ display: showList ? "none" : "flex" }} className="listBottom">
            <div className="left" onClick={this.goMyList}>
              <Badge dot={isReadFlag}>
                <img alt="" src={require("../../assets/images/list.png")} />
                <p>我的需求单</p>
              </Badge>
            </div>
            <div onClick={this.submit} className="right">
              <Button type="warning">提交</Button>
            </div>
          </div>

          <div className="replyBottom" style={{ display: showList && isReplyShow && source === "app" ? "flex" : "none" }}>
            <div className="left">
              <Input placeholder="请输入回复内容" onClick={this.replyBtn} />
            </div>
            <div onClick={this.submit} className="right">
              <Button type="warning" onClick={this.replyBtn}>
                回复
              </Button>
            </div>
          </div>
          <div className="epmy" style={{ display: showList && isReplyShow && source === "pc" ? "block" : "none" }}></div>
          <div className="replypc" style={{ display: showList && isReplyShow && source === "pc" ? "block" : "none" }}>
            <div className="pcTop">
              <p className="replyTitle">回复</p>
              <Select defaultValue="1" onChange={this.changeReply} style={{ width: "90%" }}>
                <Option value="1">需求已收到，定制服务团队会尽快与您联系，请保持电话畅通。</Option>
                <Option value="2">定制服务内容已发送给您，请注意查收。</Option>
              </Select>
            </div>
            <TextareaItem onChange={this.changeReplyCont} className="replyText" {...getFieldProps("replyCon")} rows={5} count={100} placeholder="请输入回复内容" />
            <div className="pcReplyBtn">
              <Button className="pcrebtn">取消</Button>
              <Button className="pcrebtn" type="warning" onClick={this.replyBtn}>
                回复
              </Button>
            </div>
          </div>

          <Calendar
            {...this.state.config}
            visible={this.state.mShow}
            onCancel={this.onCancel}
            onConfirm={this.onConfirm}
            onSelectHasDisableDate={this.onSelectHasDisableDate}
            getDateExtra={this.getDateExtra}
            defaultDate={now}
            minDate={new Date(+now - 5184000000)}
            maxDate={new Date(+now + 31536000000)}
            infiniteOpt={true}
            getDateExtra={this.getDateExtra}
          />

          <Modal style={{ maxWidth: "100%" }} visible={this.state.isReply} okText="回复" onOk={this.replyOk} cancelText="取消" onCancel={this.replyCancel}>
            <div className="replyModel">
              <p className="replyTitle">回复</p>
              <Select defaultValue="1" onChange={this.changeReply} style={{ width: "100%" }}>
                <Option value="1">需求已收到，定制服务团队会尽快与您联系，请保持电话畅通。</Option>
                <Option value="2">定制服务内容已发送给您，请注意查收。</Option>
              </Select>
              <TextareaItem onChange={this.changeReplyCont} className="replyText" {...getFieldProps("replyCon")} rows={5} count={100} placeholder="请输入回复内容" />
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}
let NeedListForm = createForm()(NeedList);

NeedListForm = Page(NeedListForm);
export default withRouter(NeedListForm);
