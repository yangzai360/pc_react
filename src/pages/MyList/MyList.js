import React from "react";
import Page from "../../components/Page/Page";
import { withRouter } from "react-router-dom";
import { NavBar, Icon } from "antd-mobile";
import API from "../../constants/api";
import { hasParameter, transformTimestamp, _fetch, format } from "../../utils/util";
import { FileTextTwoTone, SwapRightOutlined } from "@ant-design/icons";
import "./MyList.scss";

var ua = navigator.userAgent.toLowerCase();
var u = navigator.userAgent;
var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1; //android终端
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
if (isiOS) {
  if (window && window.webkit && window.webkit.messageHandlers) {
    window.webkit.messageHandlers.getCityLocation.postMessage(null);
  }
} else if (isAndroid) {
  window.WebApp.getCityLocation();
}
class MyList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: "",
      listData: [],
      source: "",
    };
  }
  componentDidMount() {
    var userId = hasParameter("userId", this.props);
    // var source = hasParameter("source",this.props)
    var param = { submitUserId: userId };
    if (isiOS) {
      if (window && window.webkit && window.webkit.messageHandlers) {
        window.webkit.messageHandlers.getNativeParams.postMessage(JSON.stringify({ method: API.LISTBYUSERID, data: param }));
      }
    } else if (isAndroid) {
      window.WebApp.getNativeParams(API.LISTBYUSERID, JSON.stringify(param));
    }
    setTimeout(() => {
      _fetch(API.LISTBYUSERID, "post", {
        bundleIdentifier: "com.epectravel.epec.trip",
        clienttype: 1,
        data: param,
        mcode: "ANDROID_I869019038625257",
        method: API.LISTBYUSERID,
        pid: "20180625142349344452",
        requestid: "1586258163002",
        timestamp: "2020-04-07 19:16:03",
        token: "10815ae2-f5fe-4f6f-a3cd-64d17008a9921",
        version: "7440",
        sign: "E82B2B538298BC0D01C042827433AE54",
        mCode: "ANDROID_I869019038625257",
      }).then((res) => {
        var data = res.data.content;
        this.setState({
          listData: data,
          userId: userId,
        });
      });
    }, 1000);
  }
  goListDetail = (orderId, isread) => {
    const { redirect } = this.props;
    const { userId } = this.state;
    redirect("/listInfo?orderId=" + orderId + "&isread=" + isread + "&userId=" + userId + "&source=" + "app", "right", false);
  };
  goback = () => {
    if (hasParameter("source", this.props) === "list") {
      window.history.back();
    } else {
      if (isiOS) {
        window.webkit.messageHandlers.back.postMessage(null);
      } else {
        window.WebApp.back();
      }
    }
  };
  render() {
    const { listData } = this.state;
    return (
      <div className="page MyList">
        <NavBar className="navbar trannav titledown" mode="light" icon={<Icon style={{ color: "#1b1b1b" }} type="left" />} onLeftClick={this.goback}>
          我的需求单
        </NavBar>
        <div className="content">
          {listData.map((item, index) => {
            return (
              <div className="list" key={index}>
                <div className="dataTime">
                  <p className="timetip">提交日期 {transformTimestamp(item.addTime).split("年")[1]}</p>
                </div>
                <div className="datacon" onClick={this.goListDetail.bind(this, item.demandOrderId, item.read)}>
                  <div className="top">
                    <div className={item.read ? "iconImg " : "iconImg circle"}>
                      <img alt="" src={require("../../assets/images/listCirle.png")} />
                    </div>

                    <span className="type">{item.customTypeName}</span>
                    <p className="address">
                      {item.startAddress}
                      <SwapRightOutlined className="iconLine" />
                      {item.arriveAddress}
                      <span style={{ marginLeft: "5px" }}>
                        {item.personCount}人/{format(item.budget)}元
                      </span>
                    </p>
                  </div>
                  <div className="bottom">
                    <p className="time">
                      {item.startBeginTime !== "" ? transformTimestamp(item.startBeginTime).split("年")[1] : ""} -{" "}
                      {item.startEndTime !== "" ? transformTimestamp(item.startEndTime).split("年")[1] : ""}
                      出发
                    </p>
                    <p className="statusTip" style={{ color: item.read ? "" : "#E60012" }}>
                      {item.statusName}
                    </p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

MyList = Page(MyList);
export default withRouter(MyList);
