import Loadable from "react-loadable";
import Loading from "./Loading/Loading";

// 加载路由
const Index = Loadable({
  loader: () => import("./Index/Index"),
  loading: Loading,
});
const Test = Loadable({
  loader: () => import("./Test/Test"),
  loading: Loading,
});
const SelectDept = Loadable({
  loader: () => import("./SelectDept/SelectDept"),
  loading: Loading,
});
const NeedList = Loadable({
  loader: () => import("./NeedList/NeedList"),
  loading: Loading,
});
const MyList = Loadable({
  loader: () => import("./MyList/MyList"),
  loading: Loading,
});
export default [
  {
    path: "/",
    exact: true,
    component: Index,
  },
  {
    path: "/test",
    exact: true,
    component: Test,
  },
  {
    path: "/selectDept",
    exact: true,
    component: SelectDept,
  },
  {
    path: "/needList",
    exact: true,
    component: NeedList,
  },
  {
    path: "/listInfo",
    exact: true,
    component: NeedList,
  },
  {
    path: "/mylist",
    exact: true,
    component: MyList,
  },
];
