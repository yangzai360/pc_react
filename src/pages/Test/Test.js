import React, { useState } from "react";

function Test() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>我是测试页面</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}

export default Test;
