import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./Page.scss";
import { redirect, globalLv, goBack, loadingStatus } from "../../redux/actions/main";
import { getPathName } from "../../utils/util";
let doubled = null;
let scheduledAnimationFrame = false;

let Page = WrappedComponent => {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    class extends WrappedComponent {
      componentDidMount() {
        const { onReady, offsetTop } = this.props;
        if (onReady) {
          //每次进入保持到滚动条位置
          document.getElementsByClassName("content")[0].scrollTop = offsetTop[getPathName()];
        }
        // 监听页面滚动
        if (document.getElementsByClassName("content")[0]) {
          document.getElementsByClassName("content")[0].addEventListener("scroll", this.onScrollHandle.bind(this));
        }
        this.onScrollHandle();
      }

      onScrollHandle = () => {
        const { globalLv } = this.props;
        const animation = timestamp => {
          clearTimeout(doubled);
          doubled = setTimeout(() => {
            globalLv(document.getElementsByClassName("content")[0].scrollTop, this.props);
          }, 100);
        };
        if (scheduledAnimationFrame) {
          return;
        }
        scheduledAnimationFrame = true;
        window.requestAnimationFrame(timestamp => {
          scheduledAnimationFrame = false;
          animation(timestamp);
        });
      };

      componentWillUnmount() {
        if (document.getElementsByClassName("content")[0]) {
          document.getElementsByClassName("content")[0].removeEventListener("scroll", this.onScrollHandle.bind(this));
        }
      }
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
};

const mapStateToProps = state => {
  return {
    onReady: state.redirect.onReady,
    offsetTop: state.redirect.offsetTop,
    loading: state.redirect.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    redirect: bindActionCreators(redirect, dispatch),
    globalLv: bindActionCreators(globalLv, dispatch),
    goBack: bindActionCreators(goBack, dispatch),
    loadingStatus: bindActionCreators(loadingStatus, dispatch)
  };
};

export default Page;
