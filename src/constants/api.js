const API = {
  //本地调试
  COMMON: "/common/choseStaff/queryChildDepts", //部门
  // CONSUMER: "/api/queryConsumerReport", //查询
  CONSUMER: "/financeManage/selectchailv",
  URL: "/demandorder/api",
  // URL:"http://192.168.1.190:8080/demandorder/api",
  CUSTOMTHEME: "listCustomTheme", //定制主题
  ADDDEMAND: "addDemandOrder", //新增需求单
  FINDBYORDERID: "findByOrderId", // 需求单详情
  LISTBYUSERID: "listByUserId", //查询用户已提交需求单
  SIGNREAD: "signRead", //标记订单已读
  READFLAG: "getReadFlag", //获取已读标记
  REPLY: "reply", // 回复
};
let API_EXPORT = API;
export default API_EXPORT;
