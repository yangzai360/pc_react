export const DEPTNAME = "DEPTNAME";

export const REDIRECT_PAGE = "页面跳转"; // 页面跳转
export const LIST_DATA_LV = "LIST_DATA_LV"; // 通用列表
export const GLOBAL_LV = "GLOBAL_LV"; // 全局的滚动条
export const HISTORY_URL = "HISTORY_URL"; // 页面地址
export const IS_BACK = "IS_BACK"; // 返回是否刷新页面
export const LOADING = "LOADING"; // 全局loading效果
export const APP_UPDATE = "APP_UPDATE";
export const INDEX_NUM = "INDEX_NUM"; //记住首页重要元素告诉
