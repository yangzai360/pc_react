import { combineReducers } from "redux";

import echartsData from "./echarts";
import main from "./main";

// 合并多个reducers
const allReducers = Object.assign({}, echartsData, main);
console.log("222", allReducers);
const rootReducer = combineReducers(allReducers);
export default rootReducer;
