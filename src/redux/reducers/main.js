import { REDIRECT_PAGE, HISTORY_URL, IS_BACK, GLOBAL_LV, LOADING, APP_UPDATE, INDEX_NUM } from "../../constants/actionTypes";

const redirect = (
  state = {
    direction: "none",
    url: "/",
    onReady: false,
    offsetTop: {},
    indexnum: {
      imgh: "140px",
      isfixed: false
    },
    appUpdate: {
      mustbe: 1,
      popup: 1,
      url: "",
      maxversion_code: "",
      version_note: ""
    },
    loading: false
  },
  action
) => {
  switch (action.type) {
    case REDIRECT_PAGE: {
      return {
        ...state,
        direction: action.redirect.direction
      };
    }
    case HISTORY_URL: {
      return {
        ...state,
        url: action.redirect.url
      };
    }
    case IS_BACK: {
      return {
        ...state,
        onReady: action.onReady
      };
    }
    case GLOBAL_LV: {
      return {
        ...state,
        offsetTop: action.offsetTop
      };
    }
    case LOADING: {
      return {
        ...state,
        loading: action.loading
      };
    }
    case APP_UPDATE: {
      return {
        ...state,
        appUpdate: action.appUpdate
      };
    }
    case INDEX_NUM: {
      return {
        ...state,
        indexnum: action.indexnum
      };
    }

    default:
      return state;
  }
};

export default {
  redirect
};
