import history from "../../utils/history";
import { REDIRECT_PAGE, LIST_DATA_LV, HISTORY_URL, IS_BACK, GLOBAL_LV, LOADING, APP_UPDATE, INDEX_NUM } from "../../constants/actionTypes";
import store from "../store";
import { getPathName } from "../../utils/util";

// 跳转页面
export function redirect(url, direction, isload = true) {
  return dispatch => {
    if (typeof window !== "undefined") {
      var redirect = {
        direction,
        url: window.location.href
      };
    }
    dispatch({
      type: REDIRECT_PAGE,
      redirect
    });
    dispatch({ type: HISTORY_URL, redirect });
    // 返回是否刷新页面
    if (isload) {
      dispatch({ type: IS_BACK, onReady: true });
    } else {
      dispatch({ type: IS_BACK, onReady: false });
    }

    history.push(url);
    //页面切换结束，回复默认切花方式  适应ios左滑返回
    setTimeout(() => {
      dispatch({
        type: REDIRECT_PAGE,
        redirect: {
          direction: "none"
        }
      });
    }, 1);
  };
}
// 全局的滚动条
export function globalLv(offsetTop, props) {
  return dispatch => {
    let propsNow = props;
    if (props) {
      if (props.location) {
        propsNow = props.location.pathname;
      } else {
        propsNow = props;
      }
    } else {
      propsNow = getPathName();
    }
    // console.log('记住滚动条：', propsNow, offsetTop);
    dispatch({
      type: GLOBAL_LV,
      offsetTop: {
        ...store.getState().redirect.offsetTop,
        [propsNow]: offsetTop
      }
    });
  };
}

export function clearglobalLv(offsetTop, props) {
  return dispatch => {
    dispatch({ type: GLOBAL_LV, offsetTop: {} });
  };
}

// 全局loading效果
export function loadingStatus(loading) {
  return dispatch => {
    dispatch({ type: LOADING, loading });
  };
}

// 页面切换方向
export function redirectDir(direction) {
  return dispatch => {
    dispatch({
      type: REDIRECT_PAGE,
      redirect: {
        direction
      }
    });
  };
}

// 列表页面跳转
export function redirectList(url, offsetTop) {
  return dispatch => {
    if (typeof window !== "undefined") {
      var redirect = {
        direction: "left",
        url: window.location.href
      };
    }
    dispatch({
      type: REDIRECT_PAGE,
      redirect
    });
    dispatch({ type: LIST_DATA_LV, offsetTop });
    // 记录页面地址
    dispatch({ type: HISTORY_URL, redirect });
    dispatch({ type: IS_BACK, onReady: false });
    history.push(url);
  };
}

// 返回上一页
export function goBack() {
  return dispatch => {
    if (typeof window !== "undefined") {
      var redirect = {
        direction: "right",
        url: window.location.href
      };
    }
    dispatch({
      type: REDIRECT_PAGE,
      redirect
    });
    dispatch({ type: HISTORY_URL, redirect });
    dispatch({ type: IS_BACK, onReady: true });
    history.goBack();
  };
}

export function indexNum(imgh, isfixed) {
  return dispatch => {
    dispatch({
      type: INDEX_NUM,
      indexnum: {
        imgh: imgh || store.getState().redirect.indexnum.imgh,
        isfixed: isfixed
      }
    });
  };
}

//更新app
// 更新app
export function updateAPP(data) {
  return dispatch => {
    dispatch({
      type: APP_UPDATE,
      appUpdate: data
    });
  };
}
