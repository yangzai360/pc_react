import { DEPTNAME } from "../../constants/actionTypes";
import API from "../../constants/api";
import axios from "axios";
console.log("API", API);
export function getDept(deptN0, pid) {
  console.log(111, deptN0);
  return dispatch => {
    return axios({
      method: "post",
      url: API.COMMON,
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        companyNO: "20190117170119359110",
        companyName: "易博国际旅行社（北京）有限公司",
        tmcNO: "20180625142349349999",
        deptNO: deptN0
      }
    }).then(res => {
      let data = res.data.content.outQueryDeptList;
      console.log(data);
      dispatch({
        type: DEPTNAME,
        tree: data
      });
    });
  };
}
