import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "../reducers";
console.log("rootReducer", thunk, composeWithDevTools, rootReducer);
// 日志收集中间件
// import { createLogger } from 'redux-logger';
// const logger = createLogger();
// const composeEnhancers = composeWithDevTools({
// });
// 创建store
let store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
console.log("store", store);
export default store;
