import axios from "axios";
import API from "../constants/api";

export function hasParameter(name, props) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = props.location.search.substr(1).match(reg);
  // if (r != null) return r[2];
  if (r != null) return decodeURI(r[2]);
  return "";
}

export function getPathName() {
  return window.location.hash.split("#")[1];
}

//格式化国际时间 年 月 日

export function transformTimestamp(timestamp, type) {
  var timestamp1 = new Date(timestamp).getTime();
  const date = new Date(timestamp1);
  const Y = date.getFullYear() + (type ? type : "年");
  const M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + (type ? type : "月");
  const D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + (type ? "" : "日");
  const dateString = Y + M + D;
  return dateString;
}

//格式化国际时间

export function timestampToTime(timestamp) {
  var timestamp1 = new Date(timestamp).getTime();
  const date = new Date(timestamp1);
  const Y = date.getFullYear() + "-";
  const M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + "-";
  const D = date.getDate() < 10 ? "0" + date.getDate() + " " : date.getDate() + " ";
  const H = date.getHours() + ":";
  const MI = date.getMinutes() + ":";
  const S = date.getSeconds();
  const dateString = Y + M + D + H + MI + S;
  return dateString;
}

export function _fetch(url, type, data = {}, fn) {
  var param = JSON.parse(localStorage.getItem(`${url}`)) ? JSON.parse(localStorage.getItem(`${url}`)) : data;
  return axios({
    method: type,
    url: API.URL,
    headers: {
      "Content-Type": "application/json",
    },
    data: param,
  });
}
export function format(num) {
  if (Math.abs(num) > 0) {
    return (num + "").replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, "$&,");
  }
}

export function getLast3Month() {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth() + 1; //0-11表示1-12月
  var day = now.getDate();
  var dateObj = {};
  if (parseInt(month) < 10) {
    month = "0" + month;
  }
  if (parseInt(day) < 10) {
    day = "0" + day;
  }

  dateObj.now = year + "-" + month;

  if (parseInt(month) == 1) {
    //如果是1月份，则取上一年的10月份
    dateObj.last = parseInt(year) - 1 + "-10";
    return dateObj;
  }
  if (parseInt(month) == 2) {
    //如果是2月份，则取上一年的11月份
    dateObj.last = parseInt(year) - 1 + "-11";
    return dateObj;
  }
  if (parseInt(month) == 3) {
    //如果是3月份，则取上一年的12月份
    dateObj.last = parseInt(year) - 1 + "-12";
    return dateObj;
  }

  var preSize = new Date(year, parseInt(month) - 3, 0).getDate(); //开始时间所在月的总天数
  if (preSize < parseInt(day)) {
    // 开始时间所在月的总天数<本月总天数，比如当前是5月30日，在2月中没有30，则取下个月的第一天(3月1日)为开始时间
    let resultMonth = parseInt(month) - 2 < 10 ? "0" + parseInt(month) - 2 : parseInt(month) - 2;
    dateObj.last = year + "-" + resultMonth;
    return dateObj;
  }

  if (parseInt(month) <= 10) {
    dateObj.last = year + "-0" + (parseInt(month) - 3);
    return dateObj;
  } else {
    dateObj.last = year + "-" + (parseInt(month) - 3);
    return dateObj;
  }
}

export function getLast6Month() {
  //创建现在的时间
  var data = new Date();
  //获取年
  var year = data.getFullYear();
  //获取月
  var mon = data.getMonth() + 1 < 10 ? "0" + Number(data.getMonth() + 1) : data.getMonth() + 1;
  var arry = [];
  for (var i = 0; i < 6; i++) {
    arry[i] = year + "-" + mon;

    mon = mon - 1;
    if (mon <= 0) {
      year = year - 1;
      mon = mon + 12;
    }
    if (mon < 10) {
      mon = "0" + mon;
    }
  }
  return arry;
}
