// import API from "../constants/api";

let history;
if (typeof document !== "undefined") {
  // const createBrowserHistory = require("history/createBrowserHistory").default;
  const createHashHistory = require("history").createHashHistory;
  // 判断费否是服务器端渲染
  // if (API.API_IS_SERVER) {
  //   history = createBrowserHistory();
  // } else {
  history = createHashHistory();
  // }
}

export default history;
