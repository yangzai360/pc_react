const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/common", {
      target: "http://trip-test.epectravel.com/",
      changeOrigin: true,
      secure: false,
    }),
    createProxyMiddleware("/financeManage", {
      target: "http://trip-test.epectravel.com/",
      changeOrigin: true,
      secure: false,
    }),
    createProxyMiddleware("/Consumerreportquery", {
      target: "http://192.168.87.1:8083/",
      changeOrigin: true,
      secure: false,
    }),
    createProxyMiddleware("/api", {
      target: "http://172.26.1.129:10127/",
      changeOrigin: true,
      secure: false,
    }),
    createProxyMiddleware("/account", {
      target: "http://rap2.taobao.org:38080/",
      changeOrigin: true,
      secure: false,
    }),
    createProxyMiddleware("/demandorder", {
      target: "http://39.99.132.110:10514/",
      changeOrigin: true,
      secure: false,
    })
  );
};
