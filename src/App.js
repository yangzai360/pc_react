import React, { Component } from "react";
import "./App.css";
// // 加载路由
import { Route, Switch } from "react-router-dom";
import routes from "./pages/routes";
var ua = navigator.userAgent.toLowerCase();
var u = navigator.userAgent;
var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1; //android终端
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

class App extends Component {
  componentDidMount() {
    //定位
    window.dealCityLocation = function (msg) {
      window.localStorage.setItem("dealCityLocation", msg);
    };
    // 参数
    // if(isiOS){
    //   // window.webkit.messageHandlers.getNativeParams.postMessage(null) ;
    // }else if(isAndroid){
    //     window.WebApp.getNativeParams();
    // }
    window.setNativeParams = function (msg) {
      let param = JSON.parse(msg);
      window.localStorage.setItem(`${param.method}`, msg);
    };
  }
  render() {
    return (
      <Switch>
        {routes.map((data, index) => {
          if (data.path) {
            return <Route path={data.path} exact={data.exact} component={data.component} key={index} />;
          } else {
            return <Route path={data.path} component={data.component} key={index} />;
          }
        })}
      </Switch>
    );
  }
}

export default App;
