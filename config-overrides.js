/* config-overrides.js */

const { fixBabelImports } = require("customize-cra");

module.exports = function override(config, env) {
  fixBabelImports(
    "import",
    [
      {
        libraryName: "antd",
        libraryDirectory: "es",
        style: "css"
      },
      {
        libraryName: "antd-mobile",
        libraryDirectory: "component"
      }
    ],

    config
  );

  return config;
};
